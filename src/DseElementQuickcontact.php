<?php

/**
 * 361GRAD Element Quick Contact
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementQuickcontact;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the 361GRAD Element Quick Contact.
 */
class DseElementQuickcontact extends Bundle
{
}
