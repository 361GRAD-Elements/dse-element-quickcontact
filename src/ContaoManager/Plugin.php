<?php

namespace Dse\ElementsBundle\ElementQuickcontact\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementQuickcontact;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementQuickcontact\DseElementQuickcontact::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
