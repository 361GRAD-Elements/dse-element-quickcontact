<?php

/**
 * 361GRAD Element Quick Contact
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_quickcontact'] = ['Quick contact', 'Quick contact widget.'];

$GLOBALS['TL_LANG']['tl_content']['dse_secondline']  =
    ['Headline (Line 2)', 'Here you can add a second line to the headline.'];
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Subheadline', 'Here you can add a Subheadline.'];

$GLOBALS['TL_LANG']['tl_content']['quickcontact_legend'] = 'Quick contact size';

$GLOBALS['TL_LANG']['tl_content']['dse_columnsize'] = [
    'Size',
    'Please choose column size. 12-column width or 8-column width.',
    'twelve' => '12 - column',
    'eight'  => '&nbsp;&nbsp;8 - column',
];

$GLOBALS['TL_LANG']['tl_content']['contactleft_legend'] = 'Contact left';

$GLOBALS['TL_LANG']['tl_content']['dse_locationone'] = ['Location', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_phoneone']    = ['Phone', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_mailone']     = ['E-Mail', ''];

$GLOBALS['TL_LANG']['tl_content']['contactcenter_legend'] = 'Contact center';

$GLOBALS['TL_LANG']['tl_content']['dse_locationtwo'] = ['Location', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_phonetwo']    = ['Phone', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_mailtwo']     = ['E-Mail', ''];

$GLOBALS['TL_LANG']['tl_content']['contactright_legend'] = 'Contact right';

$GLOBALS['TL_LANG']['tl_content']['dse_locationthree'] = ['Location', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_phonethree']    = ['Phone', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_mailthree']     = ['E-Mail', ''];


