<?php

/**
 * 361GRAD Element Quick Contact
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_quickcontact'] = ['Schnellkontakt', 'Schnellkontakt widget.'];

$GLOBALS['TL_LANG']['tl_content']['secondline']  =
    ['Überschrift (Zeile 2)', 'Fügt einer Überschrift eine zweite Zeile hinzu'];
$GLOBALS['TL_LANG']['tl_content']['subheadline'] =
    ['Unterüberschrift', 'Fügt eine Subheadline hinzu'];

$GLOBALS['TL_LANG']['tl_content']['quickcontact_legend'] = 'Schnellkontakt Größe';

$GLOBALS['TL_LANG']['tl_content']['dse_columnsize'] = [
    'Größe',
    'Bitte Spaltengröße angeben.',
    'twelve' => '12 - spaltig',
    'eight'  => '&nbsp;&nbsp;8 - spaltig',
];

$GLOBALS['TL_LANG']['tl_content']['contactleft_legend'] = 'Kontakt links';

$GLOBALS['TL_LANG']['tl_content']['dse_locationone'] = ['Ort', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_phoneone']    = ['Telefon', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_mailone']     = ['E-Mail', ''];

$GLOBALS['TL_LANG']['tl_content']['contactcenter_legend'] = 'Kontakt mitte';

$GLOBALS['TL_LANG']['tl_content']['dse_locationtwo'] = ['Ort', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_phonetwo']    = ['Telefon', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_mailtwo']     = ['E-Mail', ''];

$GLOBALS['TL_LANG']['tl_content']['contactright_legend'] = 'Kontakt rechts';

$GLOBALS['TL_LANG']['tl_content']['dse_locationthree'] = ['Ort', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_phonethree']    = ['Telefon', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_mailthree']     = ['E-Mail', ''];
