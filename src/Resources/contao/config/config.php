<?php

/**
 * 361GRAD Element Quick Contact
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_quickcontact'] =
    'Dse\\ElementsBundle\\ElementQuickcontact\\Element\\ContentDseQuickcontact';

// Cascading Style Sheets
$GLOBALS['TL_CSS']['dse_quickcontact'] = 'bundles/dseelementquickcontact/css/fe_ce_dse_quickcontact.css';

