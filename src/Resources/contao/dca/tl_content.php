<?php

/**
 * 361GRAD Element Quick Contact
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_quickcontact'] =
    '{type_legend},type,headline,dse_subheadline,dse_secondline;' .
    '{quickcontact_legend},dse_columnsize;' .
    '{contactleft_legend},dse_locationone,dse_phoneone,dse_mailone;' .
    '{contactcenter_legend},dse_locationtwo,dse_phonetwo,dse_mailtwo;' .
    '{contactright_legend},dse_locationthree,dse_phonethree,dse_mailthree;' .
    '{invisible_legend:hide},invisible,start,stop';

// ELement fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondline'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'clr'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_columnsize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_columnsize'],
    'inputType' => 'radio',
    'options'   => [
        'twelve', 'eight'
    ],
    'reference' => &$GLOBALS['TL_LANG']['tl_content']['dse_columnsize'],
    'eval'      => [
        'fieldType' => 'radio'
    ],
    'sql'       => "varchar(10) NOT NULL default 'twelve'"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_locationone'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_locationone'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_phoneone'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_phoneone'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50',
        'rgxp'      => 'phone'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_mailone'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_mailone'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50',
        'rgxp'      => 'email'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_locationtwo'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_locationtwo'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_phonetwo'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_phonetwo'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50',
        'rgxp'      => 'phone'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_mailtwo'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_mailtwo'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50',
        'rgxp'      => 'email'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];


$GLOBALS['TL_DCA']['tl_content']['fields']['dse_locationthree'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_locationthree'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_phonethree'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_phonethree'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50',
        'rgxp'      => 'phone'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_mailthree'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_mailthree'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50',
        'rgxp'      => 'email'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
